import React, {useEffect, useState} from 'react';
import axios from "axios";
import Header from "./components/Header";
import SearchBar from "./components/SearchBar";
import MovieCart from "./components/MovieCart";
import Loader from "./components/Loader";

const App = () => {
    const [movies, setMovies] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        setLoading(true)
        axios.get("http://localhost:3000/db.json")
            .then(({data}) => setMovies(data.films))
        setLoading(false);
    }, [])

    const search = searchValue => {
        setLoading(true)
        axios.get(`http://localhost:3000/db.json`)
            .then(({data}) => data.films.filter(item => item.search.indexOf(searchValue.toLowerCase()) > -1))
            .then(filteredMovies => setMovies(filteredMovies))
        setLoading(false);
    }


    return (
        <div className="App">
            <Header/>
            <SearchBar search={search}/>
            <div className="movies">
                {
                    loading ? <Loader />
                    :
                    movies && movies.map((item) =>
                        <MovieCart
                            key={item.id}
                            {...item}
                            // title={item.title}
                            // url={item.imageUrl}
                            // year={item.year}
                            // imdb={item.ratingIMDB}
                            // viewersRating={item.viewersRating}
                        />
                    )
                }
            </div>
        </div>
    );
}

export default App;
