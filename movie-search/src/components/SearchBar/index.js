import React, {useState} from "react";

const SearchBar = (props) => {
    const [searchValue, setSearchValue] = useState("")

    const handleSearchValue = (e) => {
        setSearchValue(e.target.value);
    }

    const searchMovie = (e) => {
        e.preventDefault();
        props.search(searchValue)
        setSearchValue("");
    }

    return (
        <div>
            <form className="search">
                <input
                    type="text"
                    value={searchValue}
                    onChange={handleSearchValue}
                />
                <input
                    onClick={searchMovie}
                    type="submit"
                    value="SEARCH"
                />
            </form>
        </div>
    )
}

export default SearchBar