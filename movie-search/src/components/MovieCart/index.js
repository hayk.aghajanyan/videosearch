import React from "react";
import notFound from '../../assets/img/no-poster-available.jpg'

const placeholder = notFound;

const MovieCart = ({title, year, imageUrl}) => {
    return (
        <div className="movie">
            <h2 className="height">{title ? title : "no title"}</h2>
            <div>
                <img src={imageUrl ? imageUrl : placeholder} alt="#"/>
            </div>
            <p>{year ? year : "unknown"}</p>
        </div>
    )
}

export default MovieCart