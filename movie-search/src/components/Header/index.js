import React from 'react'

export default function Header () {
    return (
        <header className="app-header">
            <h2>Marvel movie search app</h2>
        </header>
    )
}